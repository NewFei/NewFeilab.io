# ⭐ 审核问卷

如以前填写过审核问卷，但没任何动静的话，请重新填写这份审核卷，感谢配合！\
链接：[https://docs.qq.com/form/page/DWmJpV3V6dWFHVVBK](https://docs.qq.com/form/page/DWmJpV3V6dWFHVVBK)

<figure><img src="../.gitbook/assets/image.png" alt=""><figcaption><p>审核问卷二维码</p></figcaption></figure>
